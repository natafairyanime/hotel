package com.example.hotel.room;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@RestController
public class RoomControler {




        @Autowired
        private RoomServices service;

        @GetMapping("/room")
        public ArrayList all() {
            return new ArrayList<>(service.findAll());
        }



        @PostMapping("/room")
        public void newBook(@RequestBody Room room) {
            service.save(room);
        }

     @GetMapping("/orders/active")
 public List<Room> activeOrders() {
         return service.getRoomIsFree();
      }


    @PutMapping("/services/{id}")
    public ResponseEntity<Object> replaceOrder(@RequestBody Room newRoom,
                                               @PathVariable Long id) {


        Room emp = service.findById(id);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        }
       // emp.setState(newRoom.getStatement());
        emp.setRoom(newRoom.getRoom());
       emp.setState(newRoom.getState());
        Room updateEmployee = service.save(emp);
        return ResponseEntity.ok().body(updateEmployee);

    }




        @DeleteMapping("/rooms{id}")
        public void delete(@PathVariable Long id) {
            service.deleteById(id);
        }
    }




