package com.example.hotel.room;

import com.example.hotel.statement.Statement;

import javax.persistence.*;

@Entity
@Table(name = "ROOM")
public class Room {
    private @Id @GeneratedValue Long id;
    private String room;
    private String state;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "room_id", nullable = false)
    private Statement statement;


Room(){}

Room(String room,Statement statement,String state)
{
    this.room=room;
  //  this.statement=statement;
    this.state =state;
}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
