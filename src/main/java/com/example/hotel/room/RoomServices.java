package com.example.hotel.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomServices {

    @Autowired
    RoomRepository repository;

    public List<Room> getRoomIsFree() {
        return  repository.findAll().stream()
                .filter(room -> room.getState() == "TRUE")
                .collect(Collectors.toList());
    }

    public ArrayList findAll() {
        return new ArrayList<>(repository.findAll());
    }



    //save
    public Room save(Room emp) {
        return	repository.save(emp);
    }



    public void deleteById(Long id) {
        repository.deleteById(id);
    }


    public Room findById(Long id) {
        return repository.findById(id).orElse(null);
    }

}
