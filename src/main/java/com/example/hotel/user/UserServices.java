package com.example.hotel.user;

import com.example.hotel.room.Room;
import com.example.hotel.statement.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServices {
    @Autowired
    UserRepository repository;

    public ArrayList findAll() {
        return new ArrayList<>(repository.findAll());
    }



    //save
    public User save(User emp) {
        return	repository.save(emp);
    }



    public void deleteById(Long id) {
        repository.deleteById(id);
    }


    public User findById(Long id) {
        return repository.findById(id).orElse(null);
    }

}
