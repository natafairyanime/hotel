package com.example.hotel.user;

import com.example.hotel.statement.Statement;

import javax.persistence.*;

@Entity
@Table(name = "USER")

public class User {

    private @Id @GeneratedValue Long id;
    private String name;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", nullable = false)
    private Statement statement;

    User(){}

    User(String name,Statement statement)
    {
        this.name=name;
        this.statement=statement;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }
}
