package com.example.hotel.user;


import com.example.hotel.statement.StatementServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserControler {



    @Autowired
    private StatementServices servicesStatem;
    @Autowired
    private UserServices services;


    @GetMapping("/user/get")
        public ArrayList all() {
            return new ArrayList<>(services.findAll());
        }



        @PostMapping("/user")
        public void newClient(@RequestBody User user) {
            services.save(user);
        }


    @GetMapping("/user/active")
    public List<User> activeOrders() {
        return servicesStatem.getStatementByUser();
    }


    @PutMapping("/user/{id}/criate")
    public ResponseEntity<Object> replaceOrder(@RequestBody User newRoom,
                                               @PathVariable Long id) {


        User emp = services.findById(id);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        }
        // emp.setState(newRoom.getStatement());
        emp.setName(newRoom.getName());

        User updateEmployee = services.save(emp);
        return ResponseEntity.ok().body(updateEmployee);

    }




    @DeleteMapping("/user/{id}")
        public void delete(@PathVariable Long id) {
            services.deleteById(id);
        }



    }



