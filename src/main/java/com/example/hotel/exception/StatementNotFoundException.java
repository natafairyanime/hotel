package com.example.hotel.exception;

public class StatementNotFoundException extends RuntimeException {
    public StatementNotFoundException(Long id) {
        super("Can not found book " + id);
    }
}
