package com.example.hotel.exception;

public class RoomNotAvailableException extends Exception {
    public RoomNotAvailableException(Long id) {
        super("Room " + id + " is not available" );
    }
}
