package com.example.hotel.exception;

public class BoocNotFoundException extends RuntimeException {
    public BoocNotFoundException(Long id) {
        super("Can not found book " + id);
    }
}
