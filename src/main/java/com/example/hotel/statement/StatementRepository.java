package com.example.hotel.statement;
import org.springframework.data.jpa.repository.JpaRepository;
public interface StatementRepository extends JpaRepository<Statement, Long>  {
}
