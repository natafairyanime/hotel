package com.example.hotel.statement;

import com.example.hotel.room.Room;
import com.example.hotel.user.User;

import javax.persistence.*;

@Entity
@Table(name = "STATEMENT")
public class Statement {
    @Id @GeneratedValue Long id;
    private String taim;
    @OneToOne(optional = false, mappedBy="statement")
    public Room room;
    @OneToOne(optional = false, mappedBy="statement")
    public User user;

    Statement(){}

    Statement(Room room,User user,String taim)
    {
        this.room=room;
        this.user=user;
        this.taim=taim;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTaim() {
        return taim;
    }

    public void setTaim(String taim) {
        this.taim = taim;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "id=" + id +
                ", taim='" + taim + '\'' +
                ", room=" + room +
                ", user=" + user +
                '}';
    }
}
