package com.example.hotel.statement;

import com.example.hotel.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatementServices {
    @Autowired
    StatementRepository repository;

/*
    public List<User> getStatementByUser() {
        return repository.findAll().stream()
                .map(user -> user.getUser())
                .distinct()
                .collect(Collectors.toList());
    }
*/
    public List<User> getStatementByUser() {
        return  repository.findAll().stream().
                map(statement -> statement.getUser()).
                distinct().collect(Collectors.toList());

    }



    //save
    public Statement save(Statement emp) {
        return	repository.save(emp);
    }



    public void deleteById(Long id) {
        repository.deleteById(id);
    }


    public Statement findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public ArrayList findAll() {
        return new ArrayList<>(repository.findAll());
    }
}
