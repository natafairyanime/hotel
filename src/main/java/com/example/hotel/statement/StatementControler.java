package com.example.hotel.statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StatementControler {

    @Autowired
    private StatementServices services;

    @GetMapping("/statement")
    public List<Statement> all() {
        return services.findAll();
    }

    @PutMapping("/statement/{id}")
    public ResponseEntity<Object> replaceOrder(@RequestBody Statement newStatement,
                                               @PathVariable Long id) {

        Statement emp = services.findById(id);
        if(emp==null) {
            return ResponseEntity.notFound().build();
        }
        emp.setTaim(newStatement.getTaim());
        Statement updateEmployee=services.save(emp);
        return ResponseEntity.ok().body(updateEmployee);

    }

    @DeleteMapping("/statement/{id}")
    public void delete(@PathVariable Long id) {
        services.deleteById(id);
    }



}
